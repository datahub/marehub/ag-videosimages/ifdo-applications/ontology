# Introduction

A web application that provides machine-readable information on various versions of the iFDO standard.

### TODO
- Use Docker container, flask, Python
- provide the standard fields as one well-organized file per version (v1.0.0 and v1.1.0 for now)
- provide a per-field web endpoint for each version (as yaml/json and html)
- provide a per-subfield web endpoint for each version (as yaml/json and html)
- each endpoint includes the field name, its type, its description, which versions its included in, which fields in other standards it maps to
